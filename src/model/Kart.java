package model;

import java.util.ArrayList;
import java.util.List;

public class Kart {

    private long kartId;
    private long kartNumarasi;
    private Musteri musteri;
    private List<Hesap> hesapList;

    //<editor-fold defaultstate="collapsed" desc="Getter & Setter">
    public long getKartId() {
        return kartId;
    }

    public void setKartId(long kartId) {
        this.kartId = kartId;
    }

    public long getKartNumarasi() {
        return kartNumarasi;
    }

    public void setKartNumarasi(long kartNumarasi) {
        this.kartNumarasi = kartNumarasi;
    }

    public Musteri getMusteri() {
        return musteri;
    }

    public void setMusteri(Musteri musteri) {
        this.musteri = musteri;
    }

    public List<Hesap> getHesapList() {
        return hesapList;
    }

    public void setHesapList(List<Hesap> hesapList) {
        this.hesapList = hesapList;
    }
//</editor-fold>
}
