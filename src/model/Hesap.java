package model;

import java.util.ArrayList;
import java.util.List;

public class Hesap {

    public static final String DEFAULT = "SEÇİN",
            VADELI = "VADELİ",
            UZUN_VADELI = "UZUN VADELİ",
            KISA_VADELI = "KISA VADELİ",
            VADESIZ = "VADESİZ",
            CARI = "CARİ";

    private long hesapId;
    private String hesapFullAd;
    private String hesapTipi;
    private long hesapNo;
    private double hesapFaizOrani;
    private String sonEklenenFaizTarihi="-";
    private String bankaAdi;
    private double hesapBakiye;
    private int hesapLimiti;
    private String hesapKilitDurumu;
    private Kart kart;
    private List<HesapHareketi> hesapHareketListesi;

    //<editor-fold defaultstate="collapsed" desc="Getter & Setter">
    public long getHesapId() {
        return hesapId;
    }

    public void setHesapId(long hesapId) {
        this.hesapId = hesapId;
    }

    public String getHesapFullAd() {
        return hesapFullAd;
    }

    public void setHesapFullAd(String hesapFullAd) {
        this.hesapFullAd = hesapFullAd;
    }

    public String getHesapTipi() {
        return hesapTipi;
    }

    public void setHesapTipi(String hesapTipi) {
        this.hesapTipi = hesapTipi;
    }

    public long getHesapNo() {
        return hesapNo;
    }

    public void setHesapNo(long hesapNo) {
        this.hesapNo = hesapNo;
    }

    // Faiz Oranı sadece dışarıdan çağırılabilir, asla set edilemez. Çünkü içerde set edilmekte.
    public double getHesapFaizOrani() {
        return hesapFaizOrani;
    }

    public void setHesapFaizOrani(double hesapFaizOrani) {
        this.hesapFaizOrani = hesapFaizOrani;
    }
    
    

    public String getBankaAdi() {
        return bankaAdi;
    }

    public void setBankaAdi(String bankaAdi) {
        this.bankaAdi = bankaAdi;
    }

    public double getHesapBakiye() {
        return hesapBakiye;
    }

    public void setHesapBakiye(double hesapBakiye) {
        this.hesapBakiye = hesapBakiye;
    }

    public int getHesapLimiti() {
        return hesapLimiti;
    }

    public void setHesapLimiti(int hesapLimiti) {
        this.hesapLimiti = hesapLimiti;
    }

    public String getHesapKilitDurumu() {
        return hesapKilitDurumu;
    }

    public void setHesapKilitDurumu(String hesapKilitDurumu) {
        this.hesapKilitDurumu = hesapKilitDurumu;
    }

    public Kart getKart() {
        return kart;
    }

    public void setKart(Kart kart) {
        this.kart = kart;
    }

    public List<HesapHareketi> getHesapHareketListesi() {
        return hesapHareketListesi;
    }

    public void setHesapHareketListesi(List<HesapHareketi> hesapHareketListesi) {
        this.hesapHareketListesi = hesapHareketListesi;
    }

    public void setSonEklenenFaizTarihi(String sonEklenenFaizTarihi) {
        this.sonEklenenFaizTarihi = sonEklenenFaizTarihi;
    }

    public String getSonEklenenFaizTarihi() {
        return sonEklenenFaizTarihi;
    }

//</editor-fold>
}
