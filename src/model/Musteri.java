package model;

import java.util.ArrayList;
import java.util.List;

public class Musteri {

    private long musteriId;
    private String musteriAdi;
    private String musteriSoyadi;
    private String musteriTcNo;
    private String cinsiyet;
    private String sifre;
    private String musteriKayitTarihi;
    private List<Kart> kartList;

    //<editor-fold defaultstate="collapsed" desc="Getter & Setter">
    public long getMusteriId() {
        return musteriId;
    }

    public void setMusteriId(long musteriId) {
        this.musteriId = musteriId;
    }

    public String getMusteriAdi() {
        return musteriAdi;
    }

    public void setMusteriAdi(String musteriAdi) {
        this.musteriAdi = musteriAdi;
    }

    public String getMusteriSoyadi() {
        return musteriSoyadi;
    }

    public void setMusteriSoyadi(String musteriSoyadi) {
        this.musteriSoyadi = musteriSoyadi;
    }

    public String getMusteriTcNo() {
        return musteriTcNo;
    }

    public void setMusteriTcNo(String musteriTcNo) {
        this.musteriTcNo = musteriTcNo;
    }

    public String getCinsiyet() {
        return cinsiyet;
    }

    public void setCinsiyet(String cinsiyet) {
        this.cinsiyet = cinsiyet;
    }

    public String getSifre() {
        return sifre;
    }

    public void setSifre(String sifre) {
        this.sifre = sifre;
    }

    public List<Kart> getKartList() {
        return kartList;
    }

    public void setKartList(List<Kart> kartList) {
        this.kartList = kartList;
    }

    public void setMusteriKayitTarihi(String musteriKayitTarihi) {
        this.musteriKayitTarihi = musteriKayitTarihi;
    }

    public String getMusteriKayitTarihi() {
        return musteriKayitTarihi;
    }

//</editor-fold>
}
