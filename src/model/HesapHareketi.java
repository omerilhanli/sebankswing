package model;

public class HesapHareketi {

    public static final String MUSTERI_KAYIT="Musteri_Kayit",
            PARA_CEKME = "Para_Cekme",
            PARA_YATIRMA = "Para_Yatırma",
            PARA_TRANSFERI = "Para_Transferi",
            FAIZ_EKLE = "Faiz_Ekle",
            HESAP_KILIT = "Hesap_Koruma_Altinda",
            HESAP_SERBEST = "Hesap_Serbest";

    private long hareketId;
    private String hareketTipi;
    private String hareketDegeri;
    private String hareketTarihi;
    private String hesapKilitDurumu;
    private Hesap hesap;

    //<editor-fold defaultstate="collapsed" desc="Getter & Setter">
    public long getHareketId() {
        return hareketId;
    }

    public void setHareketId(long hareketId) {
        this.hareketId = hareketId;
    }

    public String getHareketTipi() {
        return hareketTipi;
    }

    public void setHareketTipi(String hareketTipi) {
        this.hareketTipi = hareketTipi;
    }

    public String getHareketDegeri() {
        return hareketDegeri;
    }

    public void setHareketDegeri(String hareketDegeri) {
        this.hareketDegeri = hareketDegeri;
    }

    public String getHareketTarihi() {
        return hareketTarihi;
    }

    public void setHareketTarihi(String hareketTarihi) {
        this.hareketTarihi = hareketTarihi;
    }

    public Hesap getHesap() {
        return hesap;
    }

    public void setHesap(Hesap hesap) {
        this.hesap = hesap;
    }

    public String getHesapKilitDurumu() {
        return hesapKilitDurumu;
    }

    public void setHesapKilitDurumu(String hesapKilitDurumu) {
        this.hesapKilitDurumu = hesapKilitDurumu;
    }

//</editor-fold>
}
