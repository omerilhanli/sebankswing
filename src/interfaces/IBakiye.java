
package interfaces;

public interface IBakiye {
    void onBakiyeChanged(double bakiye);
}
