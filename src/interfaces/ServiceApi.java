package interfaces;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import model.AllObject;
import model.Hesap;
import model.HesapHareketi;
import model.Musteri;
import servicemodel.Hareket;
import servicemodel.Login;

/**
 * Client tarafının servise eriştiği class.
 * Servise bağlanırken adres, http://Ip:Port/ServiceProjectName/ApplicationPath/ServicePath/ServiceMethodPath şeklinde yazılır.
 * hem client projesi hemde servis projesi aynı bilgisayarda ise, ip = localhost alınır. aksi halde 
 * iki projeninde olduğu cihazlar aynı ağda olmalıdır yani aynı modeme bağlı olmak zorundadırlar,
 * alınan servisin bulunduğu cihaz ipsi ile servise request atılabilir.
 * @author omerilhanli
 */
public class ServiceApi {

    Gson gson = new GsonBuilder().create();
    String response = "FALSE";
    
    String ip = "192.168.1.6", localIp = "localhost";

    /**
     * Servis için adres alınır, URL connection açılır ve, servise string gönderilir.
     * servisten de yine string gelir iki durumda da her iki tarafta stringler objelere map edilerek
     * istenildiği şekillerde kullanılır.
     * @param allObject 
     */
    public void musteriKayit(AllObject allObject) {
        String str = "";
        try {
            String adr = "http://"+ip+":8080/BankaServisi/inventory/musteri/kayit";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json"); // content type - json olarak belirlenir (xml yda text de olabilir)

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
            String allObjectStr = gson.toJson(allObject, AllObject.class);
            outwrite.append(allObjectStr); // tüm string akıma yazılır
            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            str = in.readLine(); // akım içinden tüm string alınır
            System.out.println("str : " + str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String girisYap(Login login) {
        try {
            String adr = "http://"+ip+":8080/BankaServisi/inventory/musteri/giris";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));

            String loginString = gson.toJson(login, Login.class);
            outwrite.append(loginString);
            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder build = new StringBuilder();
            response = in.readLine();
            System.out.println("response:" + response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    public String getParaTransferHesabimda(Hareket hareket) {
        Gson gson = new GsonBuilder().create();
        try {
            String adr = "http://"+ip+":8080/BankaServisi/inventory/musteri/paraTransferHesabimda";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
            hareket.setHareketTipi(hareket.getHareketTipi());

            String hareketStr = gson.toJson(hareket, Hareket.class);
            outwrite.append(hareketStr);
            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            response = in.readLine();
            System.out.println("response:" + response);

            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public String getParaTransferBaskaHesaba(Hareket hareket) {
        Gson gson = new GsonBuilder().create();
        try {
            String adr = "http://"+ip+":8080/BankaServisi/inventory/musteri/paraTransferBaskaHesaba";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
            String hareketStr = gson.toJson(hareket, Hareket.class);
            outwrite.append(hareketStr);
            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            response = in.readLine();
            System.out.println("response:" + response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public String hesapKilitle(Hareket hareket) {
        Gson gson = new GsonBuilder().create();
        try {
            String adr = "http://"+ip+":8080/BankaServisi/inventory/musteri/hesapKilitle";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");
            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));

            String hareketStr = gson.toJson(hareket, Hareket.class);
            outwrite.append(hareketStr);
            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            response = in.readLine();
            System.out.println("response:" + response);
            return response;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public String getHesaplar(String hesapNo) {
        try {
            String adr = "http://"+ip+":8080/BankaServisi/inventory/musteri/hesapNoBul";
            URL url = new URL(adr);
            URLConnection con = url.openConnection();
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json");

            BufferedWriter outwrite = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
            outwrite.append(hesapNo);

            outwrite.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            response = in.readLine();
            System.out.println("1212-1 : response : " + response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

}
