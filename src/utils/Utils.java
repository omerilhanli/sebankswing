package utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.AllObject;
import model.Hesap;
import model.HesapHareketi;
import model.Kart;
import model.Musteri;

public class Utils {

    public static void createMusteriFromAllObject(AllObject allObject, Musteri musteri) {
        Kart kartAll = allObject.getKart();
        Hesap hesapAll = allObject.getHesap();
        Musteri musteriAll = allObject.getMusteri();
        
        Kart kart = new Kart();
        Hesap hesap = new Hesap();
        List<HesapHareketi> hesapHareketList = new ArrayList<>();
        
        hesapHareketList = allObject.getHesapHareketListesi();
        for (HesapHareketi hesapHareketi : hesapHareketList) {
            hesapHareketi.setHesap(hesap);
        }
        hesap.setHesapHareketListesi(hesapHareketList);
        hesap.setBankaAdi(hesapAll.getBankaAdi());
        hesap.setHesapFullAd(hesapAll.getHesapFullAd());
        hesap.setHesapLimiti(hesapAll.getHesapLimiti());
        hesap.setHesapFaizOrani(hesapAll.getHesapFaizOrani());
        System.out.println("444hesapAll : " + hesapAll.getHesapBakiye());
        hesap.setHesapBakiye(hesapAll.getHesapBakiye());
        hesap.setHesapKilitDurumu(hesapAll.getHesapKilitDurumu());
        hesap.setHesapTipi(hesapAll.getHesapTipi());
        hesap.setHesapNo(hesapAll.getHesapNo());
        hesap.setSonEklenenFaizTarihi(hesapAll.getSonEklenenFaizTarihi());

        List<Hesap> hesapList = new ArrayList<>();
        hesapList.add(hesap);
        hesap.setKart(kart); //
        kart.setHesapList(hesapList);
        
        musteri.setMusteriAdi(musteriAll.getMusteriAdi());
        musteri.setMusteriSoyadi(musteriAll.getMusteriSoyadi());
        musteri.setMusteriTcNo(musteriAll.getMusteriTcNo());
        musteri.setCinsiyet(musteriAll.getCinsiyet());
        musteri.setSifre(musteriAll.getSifre());
        musteri.setMusteriKayitTarihi(musteriAll.getMusteriKayitTarihi());
        
        List<Kart> kartList = new ArrayList<>();
        kartList.add(kart);
        kart.setMusteri(musteri);
        musteri.setKartList(kartList); //
        
    }

    public static void createAllObjectFromMusteri(Musteri realMusteri, AllObject all) {
        Kart kart = new Kart();
        Hesap hesap = new Hesap();
        List<Kart> tmpKartList = realMusteri.getKartList();
        List<Hesap> tmpHesapList = realMusteri.getKartList().get(0).getHesapList();
        ArrayList<HesapHareketi> tmpHHL = (ArrayList<HesapHareketi>) tmpHesapList.get(0).getHesapHareketListesi();

        Musteri musteri = new Musteri();
        musteri.setMusteriAdi(realMusteri.getMusteriAdi());
        musteri.setMusteriSoyadi(realMusteri.getMusteriSoyadi());
        musteri.setMusteriTcNo(realMusteri.getMusteriTcNo());
        musteri.setCinsiyet(realMusteri.getCinsiyet());
        musteri.setSifre(realMusteri.getSifre());
        all.setMusteri(musteri);

        kart.setKartNumarasi(tmpKartList.get(0).getKartNumarasi());
        all.setKart(kart);

        if (tmpHesapList != null && !tmpHesapList.isEmpty()) {
            Hesap tmpHesap = tmpHesapList.get(0);
            System.out.println("222 tmpHesap : " + tmpHesap.getHesapFaizOrani());
            hesap.setBankaAdi(tmpHesap.getBankaAdi());
            hesap.setHesapFullAd(tmpHesap.getHesapFullAd());
            hesap.setHesapFaizOrani(tmpHesap.getHesapFaizOrani());
            hesap.setHesapLimiti(tmpHesap.getHesapLimiti());
            hesap.setHesapBakiye(tmpHesap.getHesapBakiye());
            hesap.setHesapKilitDurumu(tmpHesap.getHesapKilitDurumu());
            hesap.setHesapTipi(tmpHesap.getHesapTipi());
            hesap.setHesapNo(tmpHesap.getHesapNo());
            all.setHesap(hesap);
        }

        ArrayList<HesapHareketi> hesapHareketiList = new ArrayList<>();
        if (tmpHHL != null && tmpHHL.size() != 0) {
            for (HesapHareketi tmpHH : tmpHHL) {
                HesapHareketi hh = new HesapHareketi();
                hh.setHareketTipi(tmpHH.getHareketTipi());
                hh.setHareketDegeri(tmpHH.getHareketDegeri());
                hh.setHareketTarihi(tmpHH.getHareketTarihi());
                hesapHareketiList.add(hh);
            }
        }
        all.setHesapHareketListesi(hesapHareketiList);
    }
    
    public static String today(){
        return (new SimpleDateFormat("dd MMMM yyyy hh:mm:ss")).format(new Date());
    }
}
