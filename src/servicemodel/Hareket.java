package servicemodel;

import java.io.Serializable;

public class Hareket implements Serializable {

    private String sahipHesapNo;
    private String aliciHesapNo;
    private String aliciTCNo;
    private String miktar;
    private String hareketTipi;
    private String hareketKilitDurumu;

    public String getSahipHesapNo() {
        return sahipHesapNo;
    }

    public void setSahipHesapNo(String sahipHesapNo) {
        this.sahipHesapNo = sahipHesapNo;
    }

    public String getAliciHesapNo() {
        return aliciHesapNo;
    }

    public void setAliciHesapNo(String aliciHesapNo) {
        this.aliciHesapNo = aliciHesapNo;
    }

    public String getAliciTCNo() {
        return aliciTCNo;
    }

    public void setAliciTCNo(String aliciTCNo) {
        this.aliciTCNo = aliciTCNo;
    }

    public String getMiktar() {
        return miktar;
    }

    public void setMiktar(String miktar) {
        this.miktar = miktar;
    }

    public String getHareketTipi() {
        return hareketTipi;
    }

    public void setHareketTipi(String hareketTipi) {
        this.hareketTipi = hareketTipi;
    }

    public String getHareketKilitDurumu() {
        return hareketKilitDurumu;
    }

    public void setHareketKilitDurumu(String hareketKilitDurumu) {
        this.hareketKilitDurumu = hareketKilitDurumu;
    }

}
