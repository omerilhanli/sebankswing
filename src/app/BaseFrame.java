package app;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import interfaces.ServiceApi;
import model.Hesap;
import model.HesapHareketi;
import model.Kart;
import model.Musteri;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;

// BaseFrame bir taban class'ı, yani abstract (soyut) bir JFrame class'ı. 
// Her JFrame (yani ekran) Class'ı bu class'tan türetilir. 
public abstract class BaseFrame extends JFrame {

    public static final double DEFAULT_BALANCE = 500, DEFAULT_LIMIT = 750;
    public static final String TRUE = "true", FALSE = "false";
    // -------------*------------
    public static Musteri musteri;
    public static Kart kart;
    public static Hesap hesap;

    public static List<Hesap> hesapListesi;
    public static List<Kart> kartListesi;
    public static List<HesapHareketi> hesapHareketListesi;
    // -------------*------------

    // -------------*------------
    public static HesapHareketi mSonHesapHareketi;
    // -------------*------------
    
    public static ServiceApi serviceApi;
    public static Gson gson; 

    // Bu değerler test amaçlı yazıldı. DB'den gelen istekler için (DB entegre edildiğinde kadırılıcak)
//    public static Musteri musteriDiger = new Musteri();
//    public static Kart kartDiger = new Kart();
//    public static Hesap hesapDiger = new Hesap(); // bunu sabitle
//
//    public static ArrayList<Hesap> hesapListesiDiger = new ArrayList<>();
//    public static ArrayList<Kart> kartListesiDiger = new ArrayList<>();
    //

    /**
     * Her ekran isminin alınmasını sağlayan soyut methodumuz.
     * @return String
     */
    public abstract String getTitle();

    public BaseFrame() {
        // Burda tüm ekranlar için, ekran isimleri set edilir.
        setTitle(getTitle());
    }
    
    

}
