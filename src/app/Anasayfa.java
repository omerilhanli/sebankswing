package app;

import static app.BaseFrame.gson;
import static app.BaseFrame.hesap;
import static app.BaseFrame.hesapHareketListesi;
import static app.BaseFrame.mSonHesapHareketi;
import static app.BaseFrame.musteri;
import interfaces.IBakiye;
import static app.BaseFrame.serviceApi;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import model.HesapHareketi;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import model.AllObject;
import servicemodel.Hareket;
import utils.Utils;

public final class Anasayfa extends BaseFrame implements IBakiye {

    JFrame splashEkran;
    SimpleDateFormat sdfDay = new SimpleDateFormat("dd"); // tarih formatı - değiştirilebilir
    SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
    SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
    SimpleDateFormat sdfDate = new SimpleDateFormat("dd MMMM yyyy hh:mm:ss");

    public Anasayfa(JFrame ekran) {
        initComponents();
        splashEkran = ekran;

        checkFaiz();
        
        lblIsımSoyad.setText("Hoş Geldiniz " + musteri.getMusteriAdi().toUpperCase() + " " + musteri.getMusteriSoyadi().toUpperCase());
        lblGuncelBakiye.setText(hesap.getHesapBakiye() + " TL");
        lblHesapNo.setText(hesap.getHesapNo() + "");

        setLocationRelativeTo(this);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    /**
     * Bugünün tarifi ile müşteri kayıt tarihini yada en don eklenen faiz
     * tarihinini alıp, kıyaslama yaparak aradaki ay farkını faiz oranı ile
     * çarpar sonra hesab bakiyesi de ile çarparak çıkan sonucu hesaba yatırır.
     */
    public void checkFaiz() {
        int adet = 0;
        String musteriKayitTarihi = musteri.getMusteriKayitTarihi();
        String sonEklenenFaizTarihi = hesap.getSonEklenenFaizTarihi();
        if (sonEklenenFaizTarihi.equals("-")) { // standart olarak "-" eklenmiştir, 
            //ancak faiz eklenirse tarih düzgün bir tarih olarak eklenir
            try {
                Date kayitGunu = sdfDate.parse(musteriKayitTarihi); // Standart eklenen tarih Date objesine dönüştürülür.

                int kayitAy = Integer.parseInt(sdfMonth.format(kayitGunu)); // rakam olarak kayıt ay alınır
                int kayityil = Integer.parseInt(sdfYear.format(kayitGunu)); // rakam olarak kayıt yıl alınır

                Date date = new Date();
                int buAy = Integer.parseInt(sdfMonth.format(date)); // rakam olarak bu ay alınır
                int buYil = Integer.parseInt(sdfYear.format(date)); // rakam olarak bu yıl alınır

                System.out.println("buYil : " + buYil);
                System.out.println("kayityil : " + kayityil);
                
                System.out.println("buAy : " + buAy);
                System.out.println("kayitAy : " + kayitAy);
                
                if (kayityil == buYil) {
                    if (kayitAy < buAy) {
                        adet = buAy - kayitAy;
                        System.out.println("1 : adet : " + adet);
                    }
                } else if (kayityil < buYil) {
                    adet = (buYil - kayityil) * 12;
                    System.out.println("2 : adet : " + adet);
                }
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        } else {
            try {
                Date sonEklenenFaizGunu = sdfDate.parse(sonEklenenFaizTarihi);
                int sonEklenenFaizAy = Integer.parseInt(sdfMonth.format(sonEklenenFaizGunu));
                int sonEklenenFaizYil = Integer.parseInt(sdfYear.format(sonEklenenFaizGunu));

                Date date = new Date();
                int buAy = Integer.parseInt(sdfMonth.format(date)); // rakam olarak bu ay alınır
                int buYil = Integer.parseInt(sdfYear.format(date));// rakam olarak bu yıl alınır
                System.out.println("buYil : " + buYil);
                System.out.println("sonEklenenFaizYil : " + sonEklenenFaizYil);
                
                System.out.println("buAy : " + buAy);
                System.out.println("sonEklenenFaizAy : " + sonEklenenFaizAy);
                        

                if (sonEklenenFaizYil == buYil) { // karşılaştırma yapılır
                    if (sonEklenenFaizAy < buAy) {
                        adet = buAy - sonEklenenFaizAy;
                        System.out.println("3 : adet : " + adet);
                    }
                } else if (sonEklenenFaizYil < buYil) {
                    adet = (buYil - sonEklenenFaizYil) * 12;
                    System.out.println("4 : adet : " + adet);
                }
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
        }
        System.out.println("5 ADET : " + adet);
        if (adet != 0) { // gelen adet 0 değilse miktar hesaplanır
            System.out.println("adet : " + adet);
            System.out.println("hesap.getHesapBakiye() : " + hesap.getHesapBakiye());
            System.out.println("hesap.getHesapFaizOrani() : " + hesap.getHesapFaizOrani());
            double miktar = adet * hesap.getHesapBakiye() * hesap.getHesapFaizOrani();
            System.out.println("miktar : " + miktar); // 117,6
            
            faizIsle((int)Math.round(miktar)); // servise transfer işlem requesti atılır
        }

    }

    public void faizIsle(int miktar) {
        HesapHareketi hesapHareketi = new HesapHareketi();
        hesapHareketi.setHareketTipi(HesapHareketi.FAIZ_EKLE);
        hesapHareketi.setHesapKilitDurumu(HesapHareketi.HESAP_SERBEST);
        hesapHareketi.setHareketDegeri(miktar + " TL Faiz Hesabınıza Yüklenmiştir");
        // sistemin tarih bilgisi alınır hesap hareketine set edilir
        hesapHareketi.setHareketTarihi(Utils.today());
        hesapHareketListesi.add(hesapHareketi);
        hesap.setHesapHareketListesi(hesapHareketListesi);
        hesap.setSonEklenenFaizTarihi(Utils.today());
        hesap.setHesapBakiye(hesap.getHesapBakiye() + miktar);
        

        Hareket hareket = new Hareket();
        hareket.setHareketTipi(hesapHareketi.getHareketTipi());
        hareket.setHareketKilitDurumu(hesapHareketi.getHesapKilitDurumu());
        hareket.setSahipHesapNo(hesap.getHesapNo() + "");
        hareket.setMiktar(miktar + "");

        String allObjectStr = serviceApi.getParaTransferHesabimda(hareket);
        AllObject allObjectResp = gson.fromJson(allObjectStr, AllObject.class);
        Utils.createMusteriFromAllObject(allObjectResp, musteri);
        mSonHesapHareketi = hesap.getHesapHareketListesi().get(hesap.getHesapHareketListesi().size() - 1);
        
        lblGuncelBakiye.setText(hesap.getHesapBakiye() + " TL");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblIsımSoyad = new javax.swing.JLabel();
        btnCikis = new javax.swing.JButton();
        btnParaCekme = new javax.swing.JButton();
        btnParaYatirma = new javax.swing.JButton();
        btnHesapGoruntuleme = new javax.swing.JButton();
        chbxHesapKilitle = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        lblGuncelBakiye = new javax.swing.JLabel();
        btnParaTransferi = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        lblHesapNo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblIsımSoyad.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblIsımSoyad.setText("Omer İlhanlı");
        lblIsımSoyad.setBorder(javax.swing.BorderFactory.createCompoundBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(0, 204, 255), new java.awt.Color(51, 255, 204))));

        btnCikis.setText("Çıkış");
        btnCikis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCikisActionPerformed(evt);
            }
        });

        btnParaCekme.setForeground(new java.awt.Color(204, 0, 0));
        btnParaCekme.setText("Para Çekme");
        btnParaCekme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnParaCekmeActionPerformed(evt);
            }
        });

        btnParaYatirma.setForeground(new java.awt.Color(204, 0, 0));
        btnParaYatirma.setText("Para Yatırma");
        btnParaYatirma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnParaYatirmaActionPerformed(evt);
            }
        });

        btnHesapGoruntuleme.setForeground(new java.awt.Color(204, 0, 0));
        btnHesapGoruntuleme.setText("Hesap Görüntüleme");
        btnHesapGoruntuleme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHesapGoruntulemeActionPerformed(evt);
            }
        });

        chbxHesapKilitle.setBackground(new java.awt.Color(204, 255, 255));
        chbxHesapKilitle.setFont(new java.awt.Font("Lucida Grande", 0, 14)); // NOI18N
        chbxHesapKilitle.setForeground(new java.awt.Color(204, 0, 102));
        chbxHesapKilitle.setText("Hesap Kilitle");
        chbxHesapKilitle.setToolTipText("İşaretleyerek hesabınızı tüm işlemlere karşı koruyabilirsiniz");
        chbxHesapKilitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        chbxHesapKilitle.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        chbxHesapKilitle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chbxHesapKilitleActionPerformed(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Güncel Bakiye");
        jLabel1.setBorder(javax.swing.BorderFactory.createCompoundBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), javax.swing.BorderFactory.createEtchedBorder()));

        lblGuncelBakiye.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblGuncelBakiye.setText("0 TL");
        lblGuncelBakiye.setBorder(javax.swing.BorderFactory.createCompoundBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), javax.swing.BorderFactory.createEtchedBorder()));

        btnParaTransferi.setForeground(new java.awt.Color(204, 0, 0));
        btnParaTransferi.setText("Para Transferi");
        btnParaTransferi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnParaTransferiActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Hesap No");
        jLabel2.setBorder(javax.swing.BorderFactory.createCompoundBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), javax.swing.BorderFactory.createEtchedBorder()));

        lblHesapNo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblHesapNo.setBorder(javax.swing.BorderFactory.createCompoundBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), javax.swing.BorderFactory.createEtchedBorder()));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnParaYatirma, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblGuncelBakiye, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnHesapGoruntuleme, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblIsımSoyad, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnCikis, javax.swing.GroupLayout.DEFAULT_SIZE, 136, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnParaCekme, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                                    .addComponent(btnParaTransferi, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                                    .addComponent(chbxHesapKilitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblHesapNo, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(18, 18, 18))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblIsımSoyad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCikis, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                    .addComponent(lblHesapNo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblGuncelBakiye, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnParaYatirma, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnParaCekme, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnParaTransferi, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnHesapGoruntuleme, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(chbxHesapKilitle, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
        );

        btnParaTransferi.getAccessibleContext().setAccessibleName("");
        btnParaTransferi.getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCikisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCikisActionPerformed
        JOptionPane.showMessageDialog(null, "Güle güle " + musteri.getMusteriAdi().toUpperCase() + " " + musteri.getMusteriSoyadi().toUpperCase() + ", tekrar bekleriz :)");
        setVisible(false);
        splashEkran.setVisible(true);
    }//GEN-LAST:event_btnCikisActionPerformed

    private void btnParaCekmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnParaCekmeActionPerformed
        // Tüm işlemler çin hesabın kiitli olup olmadığı kontrol edilir.
        if (mSonHesapHareketi.getHareketTipi().equals(HesapHareketi.HESAP_KILIT)) {
            JOptionPane.showMessageDialog(null, "Hesabınız Kilitli! İşlem yapmadan önce kilidi kaldırın!");
        } else {
            (new ParaCekme(this)).setVisible(true);
        }

    }//GEN-LAST:event_btnParaCekmeActionPerformed

    private void btnParaYatirmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnParaYatirmaActionPerformed
        // TODO burası tekrar duzeltilicek
        if (mSonHesapHareketi.getHareketTipi().equals(HesapHareketi.HESAP_KILIT)) {
            JOptionPane.showMessageDialog(null, "Hesabınız Kilitli! İşlem yapmadan önce kilidi kaldırın!");
        } else {
            (new ParaYatirma(this)).setVisible(true);
        }
    }//GEN-LAST:event_btnParaYatirmaActionPerformed

    private void btnHesapGoruntulemeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHesapGoruntulemeActionPerformed
        (new HesapGorunteleme()).setVisible(true);
    }//GEN-LAST:event_btnHesapGoruntulemeActionPerformed


    private void chbxHesapKilitleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chbxHesapKilitleActionPerformed
        HesapHareketi hesapHareketi = new HesapHareketi();
        hesapHareketi.setHareketTarihi(mSonHesapHareketi.getHareketTarihi());
        if (chbxHesapKilitle.isSelected()) {
            hesapHareketi.setHareketTipi(HesapHareketi.HESAP_KILIT);
            hesapHareketi.setHesapKilitDurumu(HesapHareketi.HESAP_KILIT);
        } else {
            hesapHareketi.setHareketTipi(HesapHareketi.HESAP_SERBEST);
            hesapHareketi.setHesapKilitDurumu(HesapHareketi.HESAP_SERBEST);
        }
        hesapHareketi.setHareketDegeri("0");
        // her hesap kilitleme hareketi hareketlistesine eklenir
        hesapHareketListesi.add(hesapHareketi);
        hesap.setHesapHareketListesi(hesapHareketListesi);
        mSonHesapHareketi = hesapHareketi;

        Hareket hareket = new Hareket();
        hareket.setSahipHesapNo(hesap.getHesapNo() + "");
        hareket.setHareketTipi(hesapHareketi.getHareketTipi());
        hareket.setHareketKilitDurumu(hesapHareketi.getHesapKilitDurumu());
        hareket.setMiktar(hesapHareketi.getHareketDegeri());

        String hesapKilitResponse = serviceApi.hesapKilitle(hareket);
        if (hesapKilitResponse.equals(TRUE)) {
            JOptionPane.showMessageDialog(null, "Hesabınız tüm işlemlere karşı başarılı bir şekilde korunmaya alınmıştır");
        } else {
            JOptionPane.showMessageDialog(null, "Hesabınızın kilidi kaldırılmıştır");
        }

    }//GEN-LAST:event_chbxHesapKilitleActionPerformed

    private void btnParaTransferiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnParaTransferiActionPerformed
        // TODO burası tekrar duzeltilicek
        if (mSonHesapHareketi.getHareketTipi().equals(HesapHareketi.HESAP_KILIT)) {
            JOptionPane.showMessageDialog(null, "Hesabınız Kilitli! İşlem yapmadan önce kilidi kaldırın!");
        } else {
            (new ParaTransferi(this)).setVisible(true);
        }
    }//GEN-LAST:event_btnParaTransferiActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Anasayfa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Anasayfa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Anasayfa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Anasayfa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new Anasayfa().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCikis;
    private javax.swing.JButton btnHesapGoruntuleme;
    private javax.swing.JButton btnParaCekme;
    private javax.swing.JButton btnParaTransferi;
    private javax.swing.JButton btnParaYatirma;
    private javax.swing.JCheckBox chbxHesapKilitle;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel lblGuncelBakiye;
    private javax.swing.JLabel lblHesapNo;
    private javax.swing.JLabel lblIsımSoyad;
    // End of variables declaration//GEN-END:variables

    @Override
    public String getTitle() {
        return "MME Bank Anasayfa";
    }

    @Override
    public void onBakiyeChanged(double bakiye) {
        hesap.setHesapBakiye(bakiye);
        lblGuncelBakiye.setText(hesap.getHesapBakiye() + " TL");
    }
}
